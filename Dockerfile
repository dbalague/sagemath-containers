FROM sagemath/sagemath:9.5
LABEL maintainer="Daniel Balague Guardia <d.balagueguardia@tudelft.nl>"
CMD mkdir /home/sage/COURSE
ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8
ENV SHELL /bin/bash

USER sage

ENV PATH=/home/sage/sage/local/var/lib/sage/venv-python3.9.9/bin:${PATH}
ENV SAGE_ROOT=/home/sage/sage

WORKDIR ${SAGE_ROOT}

RUN sage -pip install rise \
    && sage -pip install nbgrader --no-cache-dir \
    && sage -pip install ipython --no-cache-dir \
    && jupyter nbextension install --sys-prefix --py nbgrader --overwrite \
    && jupyter nbextension enable --sys-prefix --py nbgrader \
    && jupyter serverextension enable --sys-prefix --py nbgrader \
    && mkdir -p /home/sage/.local/share/jupyter/runtime/ \
    && mkdir -p /home/sage/.sage \
    && cp -R ${SAGE_ROOT}/local/var/lib/sage/venv-python3.9.9/share/texmf /home/sage/ 

USER root 
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get -qq update \
    && apt-get -qq install -y --no-install-recommends texlive-latex-extra pandoc \
    && apt-get -qq clean \
    && rm -r /var/lib/apt/lists/* \
    && mkdir /lab \
    && chown -R sage:sage /lab
    
USER sage
WORKDIR /home/sage
