# README #

In this repo we have the Dockerfile so that one can download an image containing SageMath, the Nbgrader extension, and LaTeX + SageTeX.

## Why this image? ##

I would like to be able to work writing my exams and writing homework assingments with SageMath + Nbgrader from any of my computers without having to download, install, and set up the environment in each one of them. It is also annoying to have to reinstall software everytime you update your computer or you obtain a new one. These Docker images, together with a cloud or git service, provide such conditions. 

## SageMath Version and Other Software ##

* SageMath Container with Nbgrader extension.
* Version of Sage: 9.5
* LaTeX
* SageTex
* Pandoc
* Rise

## Downlaod ##

* You can use the image from Docker right away.

    ```docker pull dbalague/sagemath:9.5```

## Usage ##

* Using SageMath

    - To use SageMath, you can run the container with the following options:    
    ```
    docker run -p8888:8888 dbalague/sagemath:9.5 sage-jupyter
    ```

## TODO... ##      
      
* Using LaTeX
* Integration with VS Code.

To be continued...

## How to contact me? ##

* Daniel Balague Guardia
* d.balagueguardia@tudelft.nl

## References ##
- ### SageMath ###
    * This image was created using a [Docker image](https://hub.docker.com/r/sagemath/sagemath) from [SageMath](https://sagemath.org/) 